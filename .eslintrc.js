module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint',
    "space-before-blocks": false,
    "keyword-spacing":"off",
    "arrow-spacing":"off",

  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  // add your custom rules here
  rules: {
    'max-len': ["error", { "code": 400 }],
    "eol-last": 0,
    "space-before-blocks":false,
    "keyword-spacing":"off",
    "arrow-spacing":"off",
  }
}
